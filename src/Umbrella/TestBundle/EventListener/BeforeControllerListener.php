<?php

namespace Umbrella\TestBundle\EventListener;
 
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Umbrella\TestBundle\Model\InitializableControllerInterface;
 
class BeforeControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
 
        if (!is_array($controller)) {
            // not a object but a different kind of callable. Do nothing
            return;
        }
 
        $controllerObject = $controller[0];
 
        // skip initializing for exceptions
        if ($controllerObject instanceof ExceptionController) {
            return;
        }
 
        if ($controllerObject instanceof InitializableControllerInterface) {
            $controllerObject->initialize($event->getRequest());
        }
    }
}