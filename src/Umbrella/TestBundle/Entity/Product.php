<?php

namespace Umbrella\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product entity
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="Umbrella\TestBundle\Repository\ProductRepository")
 */
class Product
{
	const BASE_PRODUCTS_IMAGES_PATH = '/img/products/';
	const DEAFULT_IMAGE = '/img/default.png';
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id()
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=200, nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Length(
     *      min = "2",
     *      max = "200"
     * )
	 */
	protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	protected $description;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="image_file_name", type="string", length=200, nullable=true)
	 */
	protected $imageFileName;
	
	/**
	 *
	 * @var string 
	 */
	protected $imageUrl;
	
	/**
     * Get the image URL
     * 
     * @return String
     */
    public function getImageUrl()
	{
		if (!$this->imageUrl)
		{
			$this->imageUrl = self::DEAFULT_IMAGE;

			if ($this->imageFileName)
			{
				$this->imageUrl = self::BASE_PRODUCTS_IMAGES_PATH . $this->imageFileName;
			}
		}
		
		return $this->imageUrl;
    }   
	
	/**
	 * Set the image URL
	 * 
	 * @param string $imageUrl
	 */
	public function setImageUrl( $imageUrl )
	{
		$this->imageUrl = $imageUrl;
		
		return $this;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imageFileName
     *
     * @param string $imageFileName
     * @return Product
     */
    public function setImageFileName($imageFileName)
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * Get imageFileName
     *
     * @return string 
     */
    public function getImageFileName()
    {
        return $this->imageFileName;
    }
}
