<?php

namespace Umbrella\TestBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
	function getAllProductsForList()
	{
		$categories = $this->createQueryBuilder( 'product' )
				->orderBy( 'product.id', 'desc' )
				->getQuery()
				->getResult();
		return $categories;
	}
}
