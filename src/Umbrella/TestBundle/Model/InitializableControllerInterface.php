<?php

namespace Umbrella\TestBundle\Model;
 
use Symfony\Component\HttpFoundation\Request;
 
interface InitializableControllerInterface
{
    public function initialize(Request $request);
}