<?php

namespace Umbrella\TestBundle\Services;

use Umbrella\TestBundle\Entity\Product;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Products
{
	/**
	 * Doctrine Entity Manager
	 * 
	 * @var EntityManager 
	 */
	private $em;
	
	/**
	 * Router Service
	 * 
	 * @var Router
	 */
	private $router;
	
	/**
	 * Services Container
	 * 
	 * @var ContainerInterface 
	 */
	private $container;

	/**
	 * Service Constructor
	 *  
	 * @param \Umbrella\TestBundle\Services\EntityManager $entityManager
	 */
	public function __construct( EntityManager $entityManager, Router $router, ContainerInterface $container )
	{
		$this->em = $entityManager;
		$this->router = $router;
		$this->container = $container;
	}
	
	/**
	 * Convert products list to array
	 * 
	 * @param array of Product $products
	 * @return array
	 */
	public function productsListToArray( $products, Request $request )
	{
		$result = array();

		foreach ( $products as $product )
		{
			$result[] = $this->productToArray( $product, $request );
		}

		return $result;
	}

	/**
	 * Convert Product to array
	 * 
	 * @param \Umbrella\TestBundle\Entity\Product $product
	 * @return array
	 */
	public function productToArray( Product $product, Request $request )
	{
		return array(
			'id' => $product->getId(),
			'name' => $product->getName(),
			'description' => $product->getDescription(),
			'imageUrl' =>  dirname($request->getBaseUrl()) . $product->getImageUrl(),
		);
	}
	
	/**
	 * Save and Update product
	 * 
	 * @param \Umbrella\TestBundle\Entity\Product $product
	 * @param \Symfony\Component\Form\Form $productForm
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function processForm( Product $product, Form $productForm, Request $request )
	{
		$statusCode = $product->getId() ? 204 : 201;

		$postData = $request->request->all();
		if (isset($postData['id']))
		{
			unset($postData['id']);
		}
		
		$productForm->submit( $postData );
		if ( $productForm->isValid() )
		{
			$this->em->persist( $product );
			$this->em->flush();

			$response = new JsonResponse( array('id'=> $product->getId()  ) );
			$response->setStatusCode( $statusCode );

			return $response;
		}

		$response = new JsonResponse( array( 'errors' => $this->getErrorMessages($productForm) ) );
		$response->setStatusCode( 400 );

		return $response;
	}
	
	public function removeProduct( Product $product )
	{
		$this->em->remove( $product );
		$this->em->flush();

		$response = new Response();
		$response->setStatusCode( 200 );

		return $response;
	}
	
	/**
	 *Upload Image proccess
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function uploadImage( Request $request )
	{
		$fileName = false;
		$productId = $request->request->get('productId');
		$basePath = $this->container->getParameter('kernel.root_dir').'/../web';
		
		$uploaded = false;		
		foreach ($_FILES as $file)
		{
			$fileName = basename($file['name']);
			$uploadedFile = new UploadedFile($file['tmp_name'], 
					$file['name'], $file['type'],
                    $file['size'], $file['error'], $test = false);
			$uploadedFile->move( $basePath . Product::BASE_PRODUCTS_IMAGES_PATH, $fileName );
			$uploaded = true;
		}
		
		$saved = false;
		if ($fileName AND $productId)
		{
			$product = $this->em->getRepository('UmbrellaBundle:Product')->find($productId);
			
			if ($product)
			{
				$product->setImageFileName($fileName);
				$this->em->flush();
				
				$saved = true;
			}			
		}
		
		$code = 400;
		if ($uploaded AND $saved)
		{
			$code = 200;
		}
		
		$response = new Response();
		$response->setStatusCode( $code );
		return $response;
	}

	/**
	 * Get form error messages
	 * 
	 * @param \Symfony\Component\Form\Form $form
	 * @return array
	 */
	private function getErrorMessages( \Symfony\Component\Form\Form $form )
	{
		$errors = array();
		foreach ( $form->getErrors() as $key => $error )
		{
			$template = $error->getMessageTemplate();
			$parameters = $error->getMessageParameters();

			foreach ( $parameters as $var => $value )
			{
				$template = str_replace( $var, $value, $template );
			}

			$errors[$key] = $template;
		}

		return $errors;
	}
}
