<?php

namespace Umbrella\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Umbrella\TestBundle\Entity\Product;
use Umbrella\TestBundle\Form\ProductType;
use Umbrella\TestBundle\Model\InitializableControllerInterface;

/**
 * Constroller for products actions
 */
class ProductController extends Controller implements InitializableControllerInterface
{
	/**
	 * initialize controller function
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 */
	public function initialize( Request $request )
	{
		//Replace JSON request data
		if ( strpos( $request->headers->get( 'Content-Type' ), 'application/json' ) === 0 )
		{
			$data = json_decode( $request->getContent(), true );
			$request->request->replace( is_array( $data ) ? $data : array()  );
		}
	}

	/**
	 * Get all products
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function allAction( Request $request )
	{
		$products = $this->getDoctrine()->getRepository( 'UmbrellaBundle:Product' )->getAllProductsForList();
		$productsJson = $this->get( 'products' )->productsListToArray( $products, $request );
		return new JsonResponse( $productsJson );
	}

	/**
	 * Get product by id
	 * 
	 * @param integer $id
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * @throws NotFoundHttpException
	 */
	public function getAction( $id, Request $request )
	{
		$product = $this->getDoctrine()->getRepository( 'UmbrellaBundle:Product' )->find( $id );

		if ( !$product instanceof Product )
		{
			throw new NotFoundHttpException( 'Product not found' );
		}

		$productJson = $this->get( 'products' )->productToArray( $product, $request );
		return new JsonResponse( $productJson );
	}

	/**
	 * Create new product
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return Response
	 */
	public function newAction( Request $request )
	{
		$product = new Product();
		$form = $this->createForm( new ProductType(), $product );
		return $this->get( 'products' )->processForm( $product, $form, $request );
	}

	/**
	 * Edit product
	 * 
	 * @param integer $id
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return Response
	 * @throws NotFoundHttpException
	 */
	public function editAction( $id, Request $request )
	{
		$product = $this->getDoctrine()->getRepository( 'UmbrellaBundle:Product' )->find( $id );

		if ( !$product instanceof Product )
		{
			throw new NotFoundHttpException( 'Product not found' );
		}

		$form = $this->createForm( new ProductType(), $product );
		return $this->get( 'products' )->processForm( $product, $form, $request );
	}

	/**
	 * Remove product by id
	 * 
	 * @param integer $id
	 * @return Response
	 * @throws NotFoundHttpException
	 */
	public function removeAction( $id )
	{
		$product = $this->getDoctrine()->getRepository( 'UmbrellaBundle:Product' )->find( $id );

		if ( !$product instanceof Product )
		{
			throw new NotFoundHttpException( 'Product not found' );
		}

		return $this->get( 'products' )->removeProduct( $product );
	}
	
	/**
	 * Upload image to product
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return Response
	 */
	public function uploadImageAction( Request $request )
	{
		return $this->get( 'products' )->uploadImage( $request );		
	}

}
