<?php

namespace Umbrella\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Controller with actions for returns templates for AngularJS
 */
class DefaultController extends Controller
{
	/**
	 * Action return response with layout template for AngularJS App
	 * 
	 * @return Response
	 */
    public function indexAction()
    {
        return $this->render('UmbrellaBundle::layout.html.twig');
    }
}
