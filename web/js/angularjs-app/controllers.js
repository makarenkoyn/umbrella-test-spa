'use strict';

/* Controllers */

var umbrellaCtrls = angular.module('UmbrellaApp.controllers', []);

umbrellaCtrls.controller('ProductsCtrl', ['$scope', '$resource', function($scope, $resource) {
		var Product = $resource('app.php/api/products/:productId',
				{productId: '@id'}, {
			save: {method: 'PUT'},
			create: {method: 'POST'}
		});
		$scope.products = Product.query();

		//Init controller
		$scope.showProdcutModal = false;
		$scope.toogleProductModal = function() {
			$scope.showProdcutModal = !$scope.showProdcutModal;
		};
		$scope.cProduct = {};

		$scope.newProduct = function() {
			$scope.productModalTitle = 'New product';
			$scope.productSaveBtnTitle = 'Create';
			$scope.cProduct = {imageUrl: 'img/default.png'};
			$scope.submitProductForm = createProductAction;
			$scope.toogleProductModal();
		};

		$scope.editProduct = function(product) {
			$scope.productModalTitle = 'Edit product #' + product.id;
			$scope.productSaveBtnTitle = 'Update';
			angular.copy(product, $scope.cProduct);
			$scope.submitProductForm = updateProductAction;
			$scope.toogleProductModal();
		};

		$scope.removeProduct = function(product) {
			if (confirm('Remove this product?'))
			{
				for (var i in $scope.products)
				{
					if ($scope.products[i].id === product.id)
					{
						$scope.products[i].$remove();
						$scope.products.splice(i, 1);
					}
				}
			}
		};

		var createProductAction = function() {
			var newProduct = new Product();
			if (validateForm())
			{
				newProduct.name = $scope.cProduct.name;
				newProduct.description = $scope.cProduct.description;
				newProduct.$create(function(response) {
					newProduct.id = response.id;	
					newProduct.$get();
					$scope.products.unshift(newProduct);
					uploadProductImage(newProduct);
					$scope.showProdcutModal = false;			
				});
			}
		};

		var updateProductAction = function() {
			if (validateForm())
			{
				for (var i in $scope.products)
				{
					if ($scope.products[i].id === $scope.cProduct.id)
					{
						var editProduct = $scope.products[i];
						editProduct.name = $scope.cProduct.name;
						editProduct.description = $scope.cProduct.description;
						editProduct.$save(function(){
							editProduct.$get();
							uploadProductImage(editProduct);
						});		
					}
				}
				$scope.showProdcutModal = false;
			}
		};
		
		var validateForm = function() {
			$scope.productError = '';
			
			if (!$scope.cProduct.name)
			{
				$scope.productError = 'Type a product name';
			}
			else if ($scope.cProduct.name.length < 2 || $scope.cProduct.name.length > 200 )
			{
				$scope.productError = 'Product name must be from 2 to 200 characters';
			}
			
			return $scope.productError ? false : true;
		}

		var uploadProductImage = function( product ) {
			var fd = new FormData();
			var uploadFile = document.getElementById('image');

			if (uploadFile.files[0])
			{
				fd.append("uploadFile", uploadFile.files[0]);
				fd.append("productId", product.id);
				var xhr = new XMLHttpRequest();
				xhr.addEventListener("load", function(){ product.$get(); }, false)
				xhr.open("POST", "app.php/api/products/" + product.id + '/uploadImage');
				xhr.send(fd);
			}
			
			angular.element(uploadFile).val(null);	 		
		};
	}]);
