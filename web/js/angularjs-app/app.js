'use strict';

var app = angular.module('UmbrellaApp', [
	'ngRoute',
	'ngResource',
	'UmbrellaApp.filters',
	'UmbrellaApp.services',
	'UmbrellaApp.directives',
	'UmbrellaApp.controllers'
]);

app.config(['$locationProvider', function($locationProvider) {
		$locationProvider.html5Mode(true);
		$locationProvider.hashPrefix = '';
	}]);

app.config(['$interpolateProvider', function($interpolateProvider) {
		$interpolateProvider.startSymbol("[[");
		$interpolateProvider.endSymbol("]]");
	}]);